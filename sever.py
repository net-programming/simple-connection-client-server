import socket

def server_program():
    host = '127.0.0.1'
    port = 5000

    server_socket = socket.socket()
    server_socket.bind((host, port))

    server_socket.listen(2)
    conn, address = server_socket.accept()
    print("Connection from: " + str(address))

    num = 1
    while num <= 100:
        data = conn.recv(1024).decode()
        print("From connected user: " + str(data))
        num = int(data) + 1
        conn.send(str(num).encode())

    conn.close()

if __name__ == '__main__':
    server_program()